#!/bin/sh
yum -y install unzip
mkdir /tmp/packer
curl -L https://releases.hashicorp.com/packer/1.3.3/packer_1.3.3_linux_amd64.zip> /tmp/packer/packer_1.3.3_linux_amd64.zip
curl -L https://releases.hashicorp.com/packer/1.3.3/packer_1.3.3_SHA256SUMS |grep linux_amd > /tmp/packer/packer_1.3.3_SHA256SUMS
cd /tmp/packer
clear
sha256sum -c packer_1.3.3_SHA256SUMS
unzip packer_1.3.3_linux_amd64.zip -d /opt/rcn/packer
