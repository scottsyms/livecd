#!/bin/sh
yum -y install mongodb-server qpid-cpp-server qpid-cpp-server-linearstore pulp-server pulp-rpm-plugins pulp-docker-plugins
systemctl enable mongod
systemctl start mongod
systemctl enable qpidd
systemctl start qpidd
pulp-gen-key-pair
pulp-gen-ca-certificate
sudo -u apache pulp-manage-db
systemctl enable httpd
systemctl start httpd
systemctl enable pulp_workers
systemctl start pulp workers
systemctl enable pulp_celerybeat
systemctl start pulp_celerybeat
systemctl enable pulp_resource_manager
systemctl start pulp_resource_manager
yum -y install pulp-admin-client pulp-rpm-admin-extensions pulp-docker-admin-extensions
