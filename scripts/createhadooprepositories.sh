#!/bin/sh
# add additional repos
# Add the repo for hadoop/hdfs
cat << EOF > /etc/yum.repos.d/hadoop.repo
#VERSION_NUMBER=2.7.1.0-169
[ambari-2.7.1.0]
#json.url = http://public-repo-1.hortonworks.com/HDP/hdp_urlinfo.json
name=ambari Version - ambari-2.7.1.0
baseurl=http://public-repo-1.hortonworks.com/ambari/centos7/2.x/updates/2.7.1.0
gpgcheck=1
gpgkey=http://public-repo-1.hortonworks.com/ambari/centos7/2.x/updates/2.7.1.0/RPM-GPG-KEY/RPM-GPG-KEY-Jenkins
enabled=1
priority=1
EOF

cat << EOF > /etc/yum.repos.d/hdp.repo
#VERSION_NUMBER=3.0.1.0-187
[HDP-3.0.1.0]
name=HDP Version - HDP-3.0.1.0
baseurl=http://public-repo-1.hortonworks.com/HDP/centos7/3.x/updates/3.0.1.0
gpgcheck=1
gpgkey=http://public-repo-1.hortonworks.com/HDP/centos7/3.x/updates/3.0.1.0/RPM-GPG-KEY/RPM-GPG-KEY-Jenkins
enabled=1
priority=1
EOF

cat << EOF > /etc/yum.repos.d/hdf.repo
#VERSION_NUMBER=3.2.0.0-520
[HDF-3.2.0.0]
name=HDF Version - HDF-3.2.0.0
baseurl=http://public-repo-1.hortonworks.com/HDF/centos7/3.x/updates/3.2.0.0
gpgcheck=1
gpgkey=http://public-repo-1.hortonworks.com/HDF/centos7/3.x/updates/3.2.0.0/RPM-GPG-KEY/RPM-GPG-KEY-Jenkins
enabled=1
priority=1
EOF

cat << EOF > /etc/yum.repos.d/hdp-utils.repo
[HDP-UTILS-1.1.0.22]
name=HDP-UTILS Version - HDP-UTILS-1.1.0.22
baseurl=http://public-repo-1.hortonworks.com/HDP-UTILS-1.1.0.22/repos/centos7
gpgcheck=1
gpgkey=http://public-repo-1.hortonworks.com/HDF/centos7/3.x/updates/3.2.0.0/RPM-GPG-KEY/RPM-GPG-KEY-Jenkins
enabled=1
priority=1
EOF

