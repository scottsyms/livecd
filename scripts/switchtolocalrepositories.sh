#!/bin/sh
# add additional repos

# Import RPM keys for the prometheus application
ls /opt/rpms/keys/|xargs -i rpmkeys --import /opt/rpms/keys/{}

# Remove the yum repo depo definitions
mkdir /etc/yum.repos.d/backup
mv /etc/yum.repos.d/* /etc/yum.repos.d/backup

# Add the local repository to the yum repo config directory
cat << EOF > /etc/yum.repos.d/local.repo
[Local-Repo]
name=Local Consolidated repo
baseurl=file:///opt/rpms/
repo_gpgcheck=0
gpgcheck=0
enabled=1
EOF

yum clean all
yum grouplist
