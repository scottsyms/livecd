#!/bin/sh

# Ensure the package database exists and pull down all the rpms  from every defined yum repository in /etc/yum.repos.d.  All repos are stored in /opt/rpms.

mkdir -p /opt/rpms
yum -y update
yum -y install yum-utils createrepo clamav unzip
sed -i '/Example/d' /etc/freshclam.conf 
freshclam
ln -s /var/clamav /opt/rpms/clamav
reposync -dmlgp /opt/rpms
createrepo -g /opt/rpms/base/comps.xml /opt/rpms
clamscan -r --bell -i /

mkdir /opt/rpms/packer
curl -L https://releases.hashicorp.com/packer/1.3.3/packer_1.3.3_linux_amd64.zip> /opt/rpms/packer/packer_1.3.3_linux_amd64.zip
curl -L https://releases.hashicorp.com/packer/1.3.3/packer_1.3.3_SHA256SUMS |grep linux_amd > /opt/rpms/packer/packer_1.3.3_SHA256SUMS
cd /opt/rpms/packer
clear
sha256sum -c packer_1.3.3_SHA256SUMS
unzip packer_1.3.3_linux_amd64.zip -d /opt/rcn/packer
