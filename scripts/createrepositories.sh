#!/bin/sh
# The purpose of this script is to load the virtual machine with the repositories needed to build
# packer clients.
#


# Collect keys
mkdir -p /opt/rpms/keys
curl -L https://packagecloud.io/prometheus-rpm/release/gpgkey > /opt/rpms/keys/pm.key
curl -L https://raw.githubusercontent.com/lest/prometheus-rpm/master/RPM-GPG-KEY-prometheus-rpm > /opt/rpms/keys/pm2.key
curl -L https://download.docker.com/linux/centos/gpg > /opt/rpms/keys/dk.key
curl -L https://www.virtualbox.org/download/oracle_vbox.asc > /opt/rpms/keys/vb.key
curl -L https://www.atomicorp.com/RPM-GPG-KEY.art.txt > /opt/rpms/keys/ossec.key
curl -L https://www.atomicorp.com/RPM-GPG-KEY.atomicorp.txt > /opt/rpms/keys/ossec2.key
curl -L https://repos.fedorapeople.org/repos/pulp/pulp/GPG-RPM-KEY-pulp-2 > /opt/rpms/keys/pulp.key
curl -L https://dl.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-7 > /opt/rpms/keys/epel.key

# Import RPM keys for the prometheus application
ls /opt/rpms/keys/|xargs -i rpmkeys --import /opt/rpms/keys/{}

# Install EPEL
yum -y install http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

# Add the Pulp repository
# Pulp manages various repository types; it's included for testing purposes.
cat << EOF > /etc/yum.repos.d/pulp.repo
[pulp-2-stable]
name=Pulp 2 Production Releases
baseurl=https://repos.fedorapeople.org/repos/pulp/pulp/stable/2/\$releasever/\$basearch/
enabled=1
skip_if_unavailable=1
gpgcheck=1
gpgkey=https://repos.fedorapeople.org/repos/pulp/pulp/GPG-RPM-KEY-pulp-2
EOF

# Add the prometheus repository to the yum repo config directory
# Prometheus is a framework for managing system health information.
cat << EOF > /etc/yum.repos.d/prometheus.repo
[prometheus-rpm_release]
name=prometheus-rpm_release
baseurl=https://packagecloud.io/prometheus-rpm/release/el/7/\$basearch/
repo_gpgcheck=1
gpgcheck=1
enabled=1
gpgkey=https://packagecloud.io/prometheus-rpm/release/gpgkey
       https://raw.githubusercontent.com/lest/prometheus-rpm/master/RPM-GPG-KEY-prometheus-rpm
sslverify=1
sslcacert=/etc/pki/tls/certs/ca-bundle.crt
metadata_expire=300
EOF

# Add the repo for virtualbox
# Virtualbox is Oracle's desktop virtualisation environment
cat << EOF > /etc/yum.repos.d/virtualbox.repo
[virtualbox]
name=Oracle Linux / RHEL / CentOS-\$releasever / \$basearch - VirtualBox
baseurl=http://download.virtualbox.org/virtualbox/rpm/rhel/7/x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://www.virtualbox.org/download/oracle_vbox.asc
EOF

# Add the docker repositories
cat << EOF > /etc/yum.repos.d/docker-ce.repo
[docker-ce-stable]
name=Docker CE Stable - \$basearch
baseurl=https://download.docker.com/linux/centos/7/\$basearch/stable
enabled=1
gpgcheck=1
gpgkey=https://download.docker.com/linux/centos/gpg
EOF

# Add ossec
cat << EOF > /etc/yum.repos.d/atomic.repo
[atomic]
name = CentOS / Red Hat Enterprise Linux \$releasever - atomic
mirrorlist = http://updates.atomicorp.com/channels/mirrorlist/atomic/centos-\$releasever-\$basearch
enabled = 1
protect = 0
gpgcheck = 1
gpgkey = file:///opt/rpms/keys/ossec.key

EOF

#gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY.art.txt
#	file:///etc/pki/rpm-gpg/RPM-GPG-KEY.atomicorp.txt
