#!/bin/sh

yum -y install VirtualBox-5.2 kernel-devel
yum -y groupinstall "Development Tools"
vboxconfig
mkdir /opt/rcn/packer/iso
curl -L http://mirror.its.dal.ca/centos/7.6.1810/isos/x86_64/CentOS-7-x86_64-Minimal-1810.iso > /opt/rcn/packer/iso/centos7.iso
