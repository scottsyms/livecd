# livecd
Components for CentOS LiveCD

These are some files that are useful to create a CentOS LiveCD for hosting projects.

The intent is to create a single Linux platform that can be modified with anisble roles to fulfill all needs.

I install a minimal CentOS with the following steps.
turn on networking
date and time: Region: Etc, City: Coordinated Universal Time
Installation Destination: Done
Begin installation
Root password set
then finally reboot.

the Scripts need to be checked out into /opt/rcn so you'll need to run

yum -y install git

and

mkdir /opt/rcn

git clone https://gitlab.com/scottsyms/livecd.git /opt/rcn

Then run
cd /opt/rcn/scripts
./createrepositories.sh
./retrieverepositories.sh
./getinstalledrepo.sh
./switchtolocalrepositories.sh
./installvbox.sh
Download the latest iso to /opt/rcn/packer/iso/centos7.iso

If you want to enable nested virtualisation in HyperV, run the following in an Administrator powershell console

Set-VMProcessor -VMName <VMName> -ExposeVirtualizationExtensions $true

